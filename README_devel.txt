Copyright Nabil Alikhan 2010.
This java application can produce CGView rendered circular genome images 
based on BLAST output.

BRIG supports:
*      Genbank/EMBL
*      FASTA nucleotide; single entry or Multi-FASTA
*      FASTA protein; single entry of Multi-FASTA

CHANGE-LOG
Version 0.81
*       Fixed memory leak with formating Genbank files.
*       Fixed users mixing incompatible data in rings.
*	You can load in template image settings.
*	Minor interface bugs.
*	BRIG reverse BLAST input, showing best hits first.
*	Drooped pop-ups for Genbank sequence type, and spacer value.
	(This can be configure on the second window instead).

INSTALLATION
To run it you need to:
1.   Unzip BRIG-x.xx-dist.zip to a desired location
2.   Run BRIG.jar

LICENCE
This program is free software: you can redistribute it and/or modify it under the terms 
of the GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this 
program.  If not, see <http://www.gnu.org/licenses/>.